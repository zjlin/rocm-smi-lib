%global rocm_release 5.4
%global rocm_patch 1
%global rocm_version %{rocm_release}.%{rocm_patch}

Name:           rocm-smi-lib
Version:        %{rocm_version}
Release:        %autorelease
Summary:        ROCm System Management Interface (ROCm SMI) Library

License:        NCSA
URL:            https://github.com/RadeonOpenCompute/rocm_smi_lib
Source0:        https://github.com/RadeonOpenCompute/rocm_smi_lib/archive/refs/tags/rocm-%{rocm_version}.tar.gz#/%{name}-%{version}.tar.gz

#Patch0:         0001-add-missing-include.patch
#Patch1:         0002-fix-harden-conflict.patch
#Patch2:         0003-fix-cmake-build-type.patch

BuildRequires:  cmake
BuildRequires:  gcc-c++

#ExclusiveArch:  x86_64 aarch64 ppc64le

%description
The ROCm System Management Interface Library, or ROCm SMI library, is part
of the Radeon Open Compute ROCm software stack . It is a C library for Linux
that provides a user space interface for applications to monitor and control GPU
applications.


%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%package        -n rocm-smi
Summary:        ROCm System Management Interface
BuildArch:      noarch
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    -n rocm-smi
ROCm System Management Interface command line tools.


%prep
%autosetup
find -name '*.h' -exec chmod -x {} ';'
find -name '*.cc' -exec chmod -x {} ';'
chmod -x README.md


%build
%cmake \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DFILE_REORG_BACKWARD_COMPATIBILITY=OFF \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo
%cmake_build


%install
%cmake_install
find $RPM_BUILD_ROOT -name '*.la' -exec rm -f {} ';'
rm %{buildroot}%{_docdir}/rocm_smi/LICENSE.txt
# TODO: remove this ad-hoc version patching (the upstream pkg version gen is borken)
sed -i 's/"\.\."/"1.0.0"/g' %{buildroot}%{_libdir}/cmake/rocm_smi/rocm_smi-config-version.cmake
# fix permission
find %{buildroot}%{_libexecdir} -name '*.py' -exec chmod 0755 {} ';'

%{?ldconfig_scriptlets}


%files
%license License.txt
%doc README.md
%{_libdir}/*.so.*

%files devel
%doc README.md
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/cmake/rocm_smi/

%files -n rocm-smi
%doc README.md
%{_bindir}/*
%{_libexecdir}/*


%changelog
* Wed Apr 12 2023 Tzuchieh Lin <zjlin@zjlin.org> 5.4.1-1
- new package built with tito
- upstream version 5.4.x

%autochangelog
